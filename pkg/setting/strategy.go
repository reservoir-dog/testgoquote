package setting

import (
	"fmt"
	"os"
	"strings"
	"wangyifei_strategy/conf"
)

type Strategy struct {
	StrategyPath  string
	LogSavePath   string
	TaskName      string
	TaskCacheKey  string
	TaskParamPath string
}

var StrategySetting = &Strategy{}

func getStrategyPath() string {
	wd, err := os.Getwd()
	if err != nil {
		fmt.Println("getStrategyPath error:", err)
		return ""
	}
	fmt.Println("getStrategyPath:", wd)
	return wd
}

func getTaskName() string {
	// 获取命令行参数
	args := os.Args[1:]

	// 使用传递的参数
	strArg := "cfj_strategy"
	for _, arg := range args {
		strArg = arg
		fmt.Println("getTaskName:", arg)
	}
	return strArg
}

func getTaskCacheKey() string {
	cacheKey := strings.Replace(StrategySetting.StrategyPath, AppSetting.StrategyCodePath, "", -1) + "/" + StrategySetting.TaskName
	cacheKey = strings.Replace(cacheKey, "/", ":", -1)
	cacheKey = conf.RedisStrategyPrefix + cacheKey
	fmt.Println("getTaskCacheKey:", cacheKey)
	return cacheKey
}

func getTaskParamPath() string {
	paramPath := StrategySetting.StrategyPath + "/conf/task_param_" + StrategySetting.TaskName + ".json"
	fmt.Println("getTaskParamPath:", paramPath)
	return paramPath
}

func getLogSavePath() string {
	savePath := StrategySetting.StrategyPath + "/logs/" + StrategySetting.TaskName
	fmt.Println("getLogSavePath:", savePath)
	return savePath
}
