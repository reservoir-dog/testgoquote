package binance

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"io"
	"net/http"
	"os"
	"os/signal"
	"time"
)

const (
	pm_ws_apiBaseUrl   = "wss://fstream.binance.com/pm"
	pm_http_apiBaseUrl = "https://papi.binance.com"
)

type PmListenKeyS struct {
	ListenKey string `json:"listenKey"`
}

type BinancePmWsOrderUpdateStruct struct {
	E  string      `json:"e"`
	E1 int64       `json:"E"`
	S  string      `json:"s"`
	C  string      `json:"c"`
	S1 string      `json:"S"`
	O  string      `json:"o"`
	F  string      `json:"f"`
	Q  string      `json:"q"`
	P  string      `json:"p"`
	P1 string      `json:"P"`
	F1 string      `json:"F"`
	G  int         `json:"g"`
	C1 string      `json:"C"`
	X  string      `json:"x"`
	X1 string      `json:"X"`
	R  string      `json:"r"`
	I  int         `json:"i"`
	L  string      `json:"l"`
	Z  string      `json:"z"`
	L1 string      `json:"L"`
	N  string      `json:"n"`
	N1 interface{} `json:"N"`
	T  int64       `json:"T"`
	T1 int         `json:"t"`
	V  int         `json:"v"`
	I1 int         `json:"I"`
	W  bool        `json:"w"`
	M  bool        `json:"m"`
	M1 bool        `json:"M"`
	O1 int64       `json:"O"`
	Z1 string      `json:"Z"`
	Y  string      `json:"Y"`
	Q1 string      `json:"Q"`
	W1 int64       `json:"W"`
	V1 string      `json:"V"`
}

func CreatePmListenKey(ApiKey string) (PmListenKeyS, error) {
	http_url := fmt.Sprintf("%s/papi/v1/listenKey", pm_http_apiBaseUrl)
	client := &http.Client{}
	req, err := http.NewRequest("POST", http_url, nil)
	if err != nil {
		return PmListenKeyS{}, err
	}
	req.Header.Set("X-MBX-APIKEY", ApiKey)
	resp, err := client.Do(req)
	if err != nil {
		return PmListenKeyS{}, err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return PmListenKeyS{}, err
	}
	var lis PmListenKeyS
	err = json.Unmarshal(body, &lis)
	if err != nil {
		return PmListenKeyS{}, err
	}
	return lis, err
}

func ExtendPmListenKey(ApiKey string) (PmListenKeyS, error) {
	http_url := fmt.Sprintf("%s/papi/v1/listenKey", pm_http_apiBaseUrl)
	client := &http.Client{}
	req, err := http.NewRequest("PUT", http_url, nil)
	if err != nil {
		return PmListenKeyS{}, err
	}
	req.Header.Set("X-MBX-APIKEY", ApiKey)
	resp, err := client.Do(req)
	if err != nil {
		return PmListenKeyS{}, err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return PmListenKeyS{}, err
	}
	var lis PmListenKeyS
	err = json.Unmarshal(body, &lis)
	if err != nil {
		return PmListenKeyS{}, err
	}
	return lis, err
}

func SubscribePmPrivateInfo(ctx context.Context, apikey string,
	onPmAccountMsg func(event interface{})) error {
	lis, _ := CreatePmListenKey(apikey)
	ws_url := fmt.Sprintf("%s/ws/%s", pm_ws_apiBaseUrl, lis.ListenKey)
	fmt.Println("ws_url: %s", ws_url)
	c, _, err := websocket.DefaultDialer.Dial(ws_url, nil)
	if err != nil {
		fmt.Println("websocket.DefaultDialer.Dial error: %s", err)
		return err
	}
	currentTime := time.Now()
	formattedTime := currentTime.Format("2006-01-02 15:04:05")
	fmt.Printf("%s, SubscribePmPrivateInfo success: \n", formattedTime)
	done := make(chan struct{})
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt)

	go func() {
		defer close(done)
		for {
			_, data, err := c.ReadMessage()
			if err != nil {
				currentTime := time.Now()
				formattedTime := currentTime.Format("2006-01-02 15:04:05")
				fmt.Printf("%s, ReadJSON error: %s \n", formattedTime, err)
				return
			} else {
				var json_data interface{}
				err = json.Unmarshal(data, &json_data)
				fmt.Println("json_data: ", json_data)
				onPmAccountMsg(json_data)
			}
		}
	}()

	pongTicker := time.NewTicker(10 * time.Minute)
	defer pongTicker.Stop()

	for {
		select {
		case <-done:
			return err
		case t := <-pongTicker.C:
			lis, _ := ExtendPmListenKey(apikey)
			fmt.Printf("lis: %s \n", lis)
			err := c.WriteMessage(websocket.PongMessage, []byte(fmt.Sprintf("Pong %v", t)))
			if err != nil {
				fmt.Printf("write pong: %s \n", err)
				return err
			}
		case <-interrupt:
			fmt.Printf("interrupt \n")

			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				fmt.Printf("write close: %s \n", err)
				return err
			}
			select {
			case <-done:
			case <-time.After(time.Second):
			}
			return err
		case <-ctx.Done():
			currentTime := time.Now()
			formattedTime := currentTime.Format("2006-01-02 15:04:05")
			fmt.Printf("%s, context done: %s \n", formattedTime, ctx.Err())
			err := c.WriteMessage(websocket.CloseMessage, websocket.FormatCloseMessage(websocket.CloseNormalClosure, ""))
			if err != nil {
				fmt.Printf("write close: %s \n", err)
			}
			return ctx.Err()

		}
	}
	return err
}
