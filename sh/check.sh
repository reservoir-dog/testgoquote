#!/bin/bash

# sh sh/check.sh strategy_use_system task_1
strategy_name=$1
task_name=$2
strategy_env=$3

if [ -z "$strategy_name" ]; then
  echo "脚本没有传入第一个参数：策略名"
  exit 1
fi

if [ -z "$task_name" ]; then
  echo "脚本没有传入第二个参数：任务名"
  exit 1
fi

# if [ "$strategy_env" != "dev" ] && [ "$strategy_env" != "prod" ]; then
#   echo "脚本没有传入第三个参数：环境名(dev或prod)"
#   exit 1
# fi

# 获取脚本所在的目录路径
script_dir="$(cd "$(dirname "$0")" && pwd)"

# 切换到脚本所在的目录
cd "$script_dir"

# 切换到策略所在的根目录
cd "../"

# 获取当前目录
current_dir=$(pwd)

if [ ! -d "$current_dir/$strategy_name" ]; then
  echo "目录不存在：$current_dir/$strategy_name"
  exit 1
fi

# 切换到策略所在的目录
cd "$strategy_name"

# 获取当前目录
current_dir=$(pwd)

# 打印当前目录
# echo "当前目录：$current_dir"

proccess_path=$current_dir/build/$task_name
# 查询指定路径的进程ID
pids=$(pgrep -f "$proccess_path")
if [ -n "$pids" ]; then
#   echo "进程ID:$pids"
  exit 0
else
#   echo "未找到符合条件的进程"
  exit 1
fi