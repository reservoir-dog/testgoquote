#!/bin/bash
# sh sh/create.sh demo 
strategy_name=$1

if [ -z "$strategy_name" ]; then
  echo "脚本没有传入第一个参数：策略名"
  exit 1
fi

# 获取脚本所在的目录路径
script_dir="$(cd "$(dirname "$0")" && pwd)"

# 切换到脚本所在的目录
cd "$script_dir"

# 切换到策略所在的根目录
cd "../"

# 获取当前目录
current_dir=$(pwd)

strategy_name=strategy_$strategy_name

strategy_folder=$current_dir/$strategy_name
if [ ! -d "$strategy_folder" ]; then
  mkdir -p "$strategy_folder"
  echo "$strategy_name 文件夹已创建"
else
  echo "$strategy_name 文件夹已存在"
  exit 1
fi

# 切换到策略所在的目录
cd "$strategy_name"

# 获取当前目录
current_dir=$(pwd)

build_folder=$current_dir/build
if [ ! -d "$build_folder" ]; then
  mkdir -p "$build_folder"
  echo '*' > $build_folder/.gitignore
  echo '!.gitignore' >> $build_folder/.gitignore
  echo "build文件夹已创建"
fi

logs_folder=$current_dir/logs
if [ ! -d "$logs_folder" ]; then
  mkdir -p "$logs_folder"
  echo '*' > $logs_folder/.gitignore
  echo '!.gitignore' >> $logs_folder/.gitignore
  echo "logs文件夹已创建"
fi

conf_folder=$current_dir/conf
if [ ! -d "$conf_folder" ]; then
  mkdir -p "$conf_folder"
  echo '*' > $conf_folder/.gitignore
  echo '!.gitignore' >> $conf_folder/.gitignore
  echo "conf文件夹已创建"
fi

# 打印当前目录
echo "初始化策略目录成功：$current_dir"
