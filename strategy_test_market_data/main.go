package main

import (
	"encoding/json"
	"fmt"
	"os"
	"wangyifei_strategy/pkg/setting"
	"wangyifei_strategy/strategy_test_market_data/push_func"
	"wangyifei_strategy/utils/logger"

	"github.com/sirupsen/logrus"
)

var Logger *logrus.Entry
var Logger_l *logrus.Entry
var CurrentDir string

func init() {
	setting.Setup()
	Logger = logger.NewTerminalLogger(setting.StrategySetting.LogSavePath+"/short_term.log", logger.DebugLevel, logrus.Fields{"package": "main"})
	Logger_l = logger.NewTerminalLogger(setting.StrategySetting.LogSavePath+"/long_term.log", logger.DebugLevel, logrus.Fields{"package": "main"})
}

type StrategyParamConfig struct {
	ExchangeName  string   `json:"exchangeName"`
	SpotSymbol    string   `json:"spot_symbol"`
	UmSwapSymbol  string   `json:"um_swap_symbol"`
	CmSwapSymbol  string   `json:"cm_swap_symbol"`
	SymbolList    []string `json:"symbol_list"`
	RestartHour   int      `json:"restart_hour"`
	RestartMinute int      `json:"restart_minute"`
}

func StrategyConfigParse() (StrategyParamConfig, error) {
	filePath := fmt.Sprintf("conf/task_param_%s.json", setting.StrategySetting.TaskName)
	r, err := os.ReadFile(filePath)
	if err != nil {
		return StrategyParamConfig{}, err
	}
	var config StrategyParamConfig
	err = json.Unmarshal(r, &config)
	if err != nil {
		return StrategyParamConfig{}, err
	}
	return config, nil
}

func main() {
	strategycfg, _ := StrategyConfigParse()
	Logger_l.Infof("strategycfg: %+v", strategycfg)
	excutor := push_func.NewPushTemplate(strategycfg.ExchangeName,
		strategycfg.SpotSymbol, strategycfg.UmSwapSymbol, strategycfg.CmSwapSymbol,
		strategycfg.RestartHour, strategycfg.RestartMinute)
	excutor.InitNewPushTemplate()
	excutor.RunUntilStop()
}
