package database

import (
	"database/sql"
	"fmt"
	"time"
	"wangyifei_strategy/pkg/setting"

	"github.com/go-redis/redis"
	_ "github.com/go-sql-driver/mysql"
)

func Connect_mysql() (*sql.DB, error) {
	// dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", "rdl", "W0kwTBY5*izeQJ7B", "107.21.83.134", 13307, "rdl_trade")
	dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s",
		setting.DatabaseSetting.User,
		setting.DatabaseSetting.Password,
		setting.DatabaseSetting.Host,
		setting.DatabaseSetting.Name)
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	return db, nil
}

func Connect_hku_mysql() (*sql.DB, error) {
	// dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s", "rdl", "W0kwTBY5*izeQJ7B", "107.21.83.134", 13307, "rdl_trade")
	dsn := fmt.Sprintf("%s:%s@tcp(%s)/%s",
		"hku",
		"Zrhjn4Xn4EIH3fGV",
		setting.DatabaseSetting.Host,
		"hku_trade")
	db, err := sql.Open("mysql", dsn)
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	return db, nil
}

func Connect_myredis() (*redis.Client, error) {
	startTime := time.Now() // 记录开始时间
	client := redis.NewClient(&redis.Options{
		Addr:     setting.RedisSetting.Host,
		Password: setting.RedisSetting.Password,
		DB:       0,
	})
	_, err := client.Ping().Result()
	if err != nil {
		return nil, err
	}
	elapsedTime := time.Since(startTime) // 计算经过的时间
	fmt.Println("ConnectMyRedis elapsed time:", elapsedTime)
	return client, nil
}

func GetFromRedis(key string, client *redis.Client) (string, error) {
	val, err := client.Get(key).Result()
	if err != nil {
		return "", err
	}
	return val, nil
}

func SetToRedis(key, value string, client *redis.Client) error {
	err := client.Set(key, value, 0).Err()
	if err != nil {
		return err
	}
	return nil
}

func GetHashFromRedis(key, field string, client *redis.Client) (string, error) {
	val, err := client.HGet(key, field).Result()
	if err != nil {
		return "", err
	}
	return val, nil
}

func GetAllHashFieldsFromRedis(key string, client *redis.Client) (map[string]string, error) {
	result, err := client.HGetAll(key).Result()
	if err != nil {
		return nil, err
	}
	return result, nil
}

func SetHashToRedis(key, field, value string, client *redis.Client) error {
	err := client.HSet(key, field, value).Err()
	if err != nil {
		return err
	}
	return nil
}

func PublishToChannel(channel, message string, client *redis.Client) error {
	err := client.Publish(channel, message).Err()
	if err != nil {
		fmt.Println("Failed to publish message to channel:", err)
		return err
	}
	return nil
}

func SubscribeToChannel(channel string, onChannel func(channel, message string), client *redis.Client) error {
	pubsub := client.Subscribe(channel)
	defer pubsub.Close()

	// Channel to receive messages on
	ch := pubsub.Channel()

	// Process messages
	for msg := range ch {
		onChannel(msg.Channel, msg.Payload)
	}
	return nil
}

func InsertIntoMySQL(db *sql.DB, query string, args ...interface{}) (sql.Result, error) {
	stmt, err := db.Prepare(query)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	result, err := stmt.Exec(args...)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func QueryMySQL(db *sql.DB, query string, args ...interface{}) (*sql.Rows, error) {
	rows, err := db.Query(query, args...)
	if err != nil {
		return nil, err
	}
	return rows, nil
}

func UpdateMySQL(db *sql.DB, query string, args ...interface{}) (sql.Result, error) {
	stmt, err := db.Prepare(query) // 准备SQL语句
	if err != nil {
		return nil, err
	}
	defer stmt.Close() // 确保在函数返回后关闭statement

	result, err := stmt.Exec(args...) // 执行SQL语句
	if err != nil {
		return nil, err
	}

	return result, nil
}
